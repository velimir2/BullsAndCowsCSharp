﻿using System;
using System.Linq;

namespace BullsAndCowsConsoleApp
{
    class Program
    {
        private static readonly Random random = new Random(Guid.NewGuid().GetHashCode());

        static void Main()
        {
            bool repeat = true;

            while (repeat)
            {
                var all = Numbers.GetAll();
                //Console.WriteLine("All possibilities: ");
                //foreach (var a in all)
                //{
                //    Console.WriteLine(a);
                //}

                var bulls = 0;
                var cows = 0;
                var guess = string.Empty;

                var numToGuess = GetRandomElementFrom(all);
                Console.WriteLine($"\n\nNumber to guess = {numToGuess}\n");

                PrintNumberOfPossibilitiesLeft(all);

                for (var i = 0; i < 1000 && bulls < 4; i++)
                {
                    guess = GetRandomElementFrom(all);
                    Numbers.FindBullsCows(guess, numToGuess, out bulls, out cows);
                    Console.WriteLine($"{i + 1}: guess = {guess}, bulls = {bulls}, cows = {cows}");

                    //Here is the MAIN LOGIC
                    all = all.Where(possible => Numbers.CheckFits(guess, possible, bulls, cows)).ToArray();

                    PrintNumberOfPossibilitiesLeft(all);
                }

                Console.WriteLine($"Guessed: {guess}!");

                Console.WriteLine("\nExit? y/n");
                if (Console.ReadKey().KeyChar == 'y')
                    repeat = false;
                }
        }

        private static string GetRandomElementFrom(string[] all)
        {
            return all[random.Next(all.Length)];
        }

        private static void PrintNumberOfPossibilitiesLeft(string[] all)
        {
            Console.WriteLine($"\t\tNumer of possibilities left = {all.Length}");
        }
    }
}
