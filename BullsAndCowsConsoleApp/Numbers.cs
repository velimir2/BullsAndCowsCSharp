﻿using System.Collections.Generic;
using System.Linq;

namespace BullsAndCowsConsoleApp
{
    public static class Numbers
    {
        public static readonly string[] All = CreateAll().ToArray();

        public static string[] GetAll()
        {
            return (string[])All.Clone();
        }

        public static bool CheckFits(string trial, string possible, int bulls, int cows)
        {
            FindBullsCows(trial, possible, out var actualBulls, out var actualCows);
            return actualBulls == bulls && actualCows == cows;
        }

        public static void FindBullsCows(string trial, string possible, out int actualBulls, out int actualCows)
        {
            actualBulls = 0;
            actualCows = 0;
            for (int i = 0; i < 4; i++)
            {
                var c = trial[i];
                if (!possible.Contains(c))
                    continue;
                if (possible[i] == c)
                    actualBulls++;
                else
                    actualCows++;
            }
        }

        private static IEnumerable<string> CreateAll()
        {
            var possibleChars = new[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

            foreach (var c0 in possibleChars.Skip(1)) //skip 0, cannot start with 0
            {
                var chars1 = possibleChars.Where(c => c != c0).ToArray();
                foreach (var c1 in chars1)
                {
                    var chars2 = chars1.Where(c => c != c1).ToArray();
                    foreach (var c2 in chars2)
                    {
                        var chars3 = chars2.Where(c => c != c2).ToArray();
                        foreach (var c3 in chars3)
                        {
                            yield return c0.ToString() + c1 + c2 + c3;
                        }
                    }
                }
            }
        }
    }
}